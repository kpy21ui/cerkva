from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^adm-in/', include(admin.site.urls)),
    url(r'^photo-albums/', include('photo.urls')),
    url(r'^contacts/', TemplateView.as_view(template_name="contacts.html"), name='contact'),
    url(r'^google223b98881eef32d3.html', TemplateView.as_view(template_name="google223b98881eef32d3.html")),
    url(r'^web-sites/', TemplateView.as_view(template_name="web-sites.html")),
    url(r'^priors/', include('about.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'news/', include('article.urls')),
    url(r'^event/', include('event.urls')),
    url(r'^', include('article.urls')),
]

urlpatterns += patterns('', (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)