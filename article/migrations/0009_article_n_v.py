# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-12 20:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0008_auto_20151212_2221'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='n_v',
            field=models.IntegerField(default=0),
        ),
    ]
