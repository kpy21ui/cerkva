# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='article_date',
            field=models.DateTimeField(verbose_name='Дата публікації'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='article_text',
            field=models.TextField(blank=True, verbose_name='Текст'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='article_title',
            field=models.CharField(max_length=200, blank=True, verbose_name='Заголовок'),
            preserve_default=True,
        ),
    ]
