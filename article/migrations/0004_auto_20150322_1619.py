# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0003_auto_20150310_1702'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='article_thumbnail',
            field=models.ImageField(null=True, blank=True, upload_to='images/articles/%Y/%m/%d'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='article_image',
            field=models.ImageField(blank=True, verbose_name='Картинка', upload_to='images/articles/%Y/%m/%d'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='article_link_on_original',
            field=models.URLField(blank=True, verbose_name='Посилання на оригінал'),
            preserve_default=True,
        ),
    ]
