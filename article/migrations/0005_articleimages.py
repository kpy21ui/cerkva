# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0004_auto_20150322_1619'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArticleImages',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('article_img', models.ImageField(blank=True, upload_to='images/articles/%Y/%m/%d', null=True)),
                ('article_thumb', models.ImageField(blank=True, upload_to='images/articles/%Y/%m/%d', null=True)),
                ('article', models.ForeignKey(to='article.Article')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
