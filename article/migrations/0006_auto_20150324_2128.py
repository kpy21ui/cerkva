# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0005_articleimages'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='articleimages',
            options={'verbose_name': 'Внутрішня фотографія новини', 'verbose_name_plural': 'Внутрішні фотографії новини'},
        ),
        migrations.AlterField(
            model_name='article',
            name='article_full_text',
            field=models.TextField(verbose_name='повний текст', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='article_news_type',
            field=models.CharField(verbose_name='Тип новини', max_length=1, choices=[('Ц', 'З життя Церкви'), ('Є', 'З життя Єпархії'), ('Д', 'З життя Деканату'), ('П', 'З життя Парафії')], default='П'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='article_short_text',
            field=models.CharField(verbose_name='Короткий опис', max_length=250, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='articleimages',
            name='article_img',
            field=models.ImageField(verbose_name='Внутрішня фотографія новини', upload_to='images/articles/%Y/%m/%d', null=True, blank=True),
            preserve_default=True,
        ),
    ]
