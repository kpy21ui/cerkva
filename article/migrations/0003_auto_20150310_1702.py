# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0002_auto_20150111_1943'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='article_text',
            new_name='article_full_text',
        ),
        migrations.AddField(
            model_name='article',
            name='article_image',
            field=models.ImageField(verbose_name='Картинка', upload_to='images/', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='article_link_on_original',
            field=models.CharField(verbose_name='Посилання на оригінал', max_length=250, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='article_news_type',
            field=models.CharField(max_length=1, choices=[('Ц', 'З життя Церкви'), ('Є', 'З життя Єпархії'), ('Д', 'З життя Деканату'), ('П', 'З життя Парафії')], default='П'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='article_short_text',
            field=models.CharField(verbose_name='Опис', max_length=250, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='article_date',
            field=models.DateTimeField(verbose_name='Дата публікації', default=datetime.datetime.now),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='article',
            name='article_title',
            field=models.CharField(verbose_name='Заголовок', max_length=250, blank=True),
            preserve_default=True,
        ),
    ]
