from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView
from cerkva.settings import MEDIA_URL
from article.models import Article, ArticleImages


class ArticlesView(ListView):
    context_object_name = 'articles'
    model = Article
    template_name = 'articles.html'
    paginate_by = 5
    media_url = MEDIA_URL
    news_type = 'parish'

    def get_queryset(self):
        news_type = None
        try:
            if self.kwargs['news_type']:
                news_type = self.kwargs['news_type']
        except KeyError:
            news_type = self.news_type

        if news_type == 'patriarch':
            qs = Article.objects.filter(article_news_type='Ц').order_by('-article_date')
        elif news_type == 'eparchy':
            qs = Article.objects.filter(article_news_type='Є').order_by('-article_date')
        elif news_type == 'deanery':
            qs = Article.objects.filter(article_news_type='Д').order_by('-article_date')
        else:
            qs = Article.objects.filter(article_news_type='П').order_by('-article_date')
        return qs

    def get_context_data(self, **kwargs):
        context = super(ArticlesView, self).get_context_data(**kwargs)
        context.update({'media_url': self.media_url, 'news_type': self.news_type})
        return context


class IndexView(ArticlesView):
    pass


class ArticleDetailView(DetailView):
    context_object_name = 'article'
    model = Article
    template_name = 'article.html'

    def get_object(self, queryset=None):
        return get_object_or_404(Article, id=self.kwargs["article_id"])

    def render_to_response(self, context, **response_kwargs):
        response = super(ArticleDetailView, self).render_to_response(context, **response_kwargs)
        if self.kwargs['article_id'] not in self.request.COOKIES:
            obj = self.get_object()
            obj.number_of_views += 1
            obj.save()
            response.set_cookie(self.kwargs['article_id'], "cerkva.sokolivka")
        return response

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        context['article_images'] = ArticleImages.objects.filter(article=self.object)
        context['media_url'] = MEDIA_URL
        return context
