from datetime import datetime
from django.db import models
# -*- coding: utf-8 -*-
import os
from PIL import Image as PImage
from cerkva.settings import MEDIA_ROOT
from tempfile import NamedTemporaryFile
from django.core.files import File
from tinymce.models import HTMLField


class Article(models.Model):
    NEWS_TYPES = (
        (u'Ц', u'З життя Церкви'),
        (u'Є', u'З життя Єпархії'),
        (u'Д', u'З життя Деканату'),
        (u'П', u'З життя Парафії'),
    )

    article_title = models.CharField(max_length=250, blank=True, verbose_name="Заголовок")
    article_image = models.ImageField(upload_to='images/articles/%Y/%m/%d', blank=True, verbose_name="Картинка")
    article_thumbnail = models.ImageField(upload_to="images/articles/%Y/%m/%d", blank=True, null=True)
    article_short_text = models.CharField(max_length=250, blank=True, verbose_name="Короткий опис")
    article_full_text = HTMLField(blank=True, verbose_name="повний текст")
    article_date = models.DateTimeField(default=datetime.now, verbose_name="Дата публікації")
    article_link_on_original = models.URLField(blank=True, verbose_name="Посилання на оригінал")
    article_news_type = models.CharField(max_length=1, choices=NEWS_TYPES, default='П', verbose_name="Тип новини")
    number_of_views = models.IntegerField(default=0, verbose_name="Кількість переглядів")

    def save(self, *args, **kwargs):
        """seve image demensions"""
        # try:
        #     old_name = Article.objects.get(id=self.id).article_image.name
        #     os.remove(os.path.join(MEDIA_ROOT, old_name))
        # except Exception:
        #     pass
        super(Article, self).save(*args, **kwargs)
        im = PImage.open(os.path.join(MEDIA_ROOT, self.article_image.name))
        im.thumbnail((1200, 1200), PImage.ANTIALIAS)
        self.width, self.height = im.size
        im.save(os.path.join(MEDIA_ROOT, self.article_image.name), "JPEG")

        # large thumbnail
        fn, ext = os.path.splitext(self.article_image.name)
        im.thumbnail((315, 210), PImage.ANTIALIAS)
        thumb_fn = fn + "-thumb2" + ext
        tf2 = NamedTemporaryFile()
        im.save(tf2.name, "JPEG")
        self.article_thumbnail.save(thumb_fn, File(open(tf2.name, 'rb')), save=False)
        tf2.close()
        super(Article, self).save(*args, **kwargs)

    def thumbnail(self):
        return """<a href="/media/%s"><img border="0" alt="" src="/media/%s" height="40" /></a>""" \
               % ((self.article_image.name, self.article_image.name))
    thumbnail.allow_tags = True

    def __str__(self):
        return self.article_title

    class Meta():
        db_table = 'article'
        verbose_name_plural = "Новини"
        verbose_name = "Новина"


class ArticleImages(models.Model):
    article = models.ForeignKey(Article)
    article_img = models.ImageField(upload_to="images/articles/%Y/%m/%d", blank=True, null=True,
                                    verbose_name="Внутрішня фотографія новини")
    article_thumb = models.ImageField(upload_to="images/articles/%Y/%m/%d", blank=True, null=True)

    def save(self, *args, **kwargs):
        """seve image demensions"""
        try:
            old_name = ArticleImages.objects.get(id=self.id).article_img.name
            os.remove(os.path.join(MEDIA_ROOT, old_name))
        except Exception:
            pass
        super(ArticleImages, self).save(*args, **kwargs)
        im = PImage.open(os.path.join(MEDIA_ROOT, self.article_img.name))
        im.thumbnail((1200, 1200), PImage.ANTIALIAS)
        self.width, self.height = im.size
        im.save(os.path.join(MEDIA_ROOT, self.article_img.name), "JPEG")

        # large thumbnail
        fn, ext = os.path.splitext(self.article_img.name)
        im.thumbnail((315, 210), PImage.ANTIALIAS)
        thumb_fn = fn + "-thumb2" + ext
        tf2 = NamedTemporaryFile()
        im.save(tf2.name, "JPEG")
        self.article_thumb.save(thumb_fn, File(open(tf2.name, 'rb')), save=False)
        tf2.close()
        super(ArticleImages, self).save(*args, **kwargs)

    class Meta():
        verbose_name_plural = "Внутрішні фотографії новини"
        verbose_name = "Внутрішня фотографія новини"