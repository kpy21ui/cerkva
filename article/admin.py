from django.contrib import admin
from article.models import Article, ArticleImages


class ArticleInline(admin.StackedInline):
    model = ArticleImages
    exclude = ['article_thumb']
    extra = 3


class ArticleAdmin(admin.ModelAdmin):
    exclude = ['article_thumbnail', 'number_of_views']
    list_display = ["__str__", "thumbnail", "article_date",
                    'article_news_type', 'number_of_views']
    search_fields = ['title', ]
    list_filter = ['article_date']
    inlines = [ArticleInline]


admin.site.register(Article, ArticleAdmin)
