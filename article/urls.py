from django.conf.urls import url
from .views import ArticlesView, ArticleDetailView, IndexView

urlpatterns = [
    url(r'^news/(?P<news_type>\w+)/(?P<article_id>\d+)/$', ArticleDetailView.as_view(), name="article-detail"),
    url(r'news/(?P<news_type>\w+)$', ArticlesView.as_view(), name="news"),
    url(r'^$', IndexView.as_view(), name="index"),
]
