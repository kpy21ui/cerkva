from django.db import models
from django.utils import timezone
from datetime import datetime


class Event(models.Model):
    EVENT_TYPE = (
        ('Л', 'Літургія'),
        ('В', 'Весілля'),
        ('П', 'Похорон')
    )
    default_event_time = datetime.strptime("10:00:00", "%H:%M:%S")
    event_type = models.CharField(max_length=1, choices=EVENT_TYPE, default='Л', verbose_name="Тип події")
    event_name = models.CharField(max_length=100, verbose_name="Назва події", blank=True)
    event_date = models.DateField(verbose_name="Дата події", default=timezone.now)
    event_time = models.TimeField(verbose_name="Час події", default=default_event_time)
    event_description = models.CharField(max_length=250, blank=True, verbose_name="Короткий опис")

    def __str__(self):
        return self.event_name

    class Meta():
        db_table = 'event'
        verbose_name_plural = "Події"
        verbose_name = "Подія"