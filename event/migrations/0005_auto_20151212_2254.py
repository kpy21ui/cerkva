# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0004_auto_20150408_1249'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='event_date',
            field=models.DateField(verbose_name='Дата події', default=datetime.datetime(2015, 12, 12, 22, 54, 36, 676201)),
            preserve_default=True,
        ),
    ]
