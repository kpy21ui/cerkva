# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('event_type', models.CharField(verbose_name='Тип події', max_length=1, choices=[('Р', 'Ранішня літургія'), ('Л', 'Літургія'), ('Н', 'Вечірня'), ('В', 'Весілля'), ('П', 'Похорон')], default='Л')),
                ('event_description', models.CharField(verbose_name='Короткий опис', max_length=250, blank=True)),
                ('event_date', models.DateTimeField(verbose_name='Дата події')),
            ],
            options={
                'verbose_name': 'Подія',
                'verbose_name_plural': 'Події',
                'db_table': 'event',
            },
            bases=(models.Model,),
        ),
    ]
