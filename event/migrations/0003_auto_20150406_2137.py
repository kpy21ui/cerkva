# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0002_auto_20150324_2257'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='event_time',
        ),
        migrations.AddField(
            model_name='event',
            name='event_name',
            field=models.CharField(max_length=100, blank=True, verbose_name='Назва події'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='event_date',
            field=models.DateTimeField(verbose_name='Дата події', default=datetime.datetime.now),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='event_type',
            field=models.CharField(choices=[('Л', 'Літургія'), ('В', 'Весілля'), ('П', 'Похорон')], max_length=1, verbose_name='Тип події', default='Л'),
            preserve_default=True,
        ),
    ]
