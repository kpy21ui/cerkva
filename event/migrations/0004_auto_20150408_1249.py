# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0003_auto_20150406_2137'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='event_time',
            field=models.TimeField(default='10:00:00', verbose_name='Час події'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='event_date',
            field=models.DateField(default=datetime.datetime(2015, 4, 8, 12, 49, 6, 586503), verbose_name='Дата події'),
            preserve_default=True,
        ),
    ]
