# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0002_auto_20150324_2257'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='event_name',
            field=models.CharField(max_length=100, verbose_name='Назва події', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='event_date',
            field=models.DateField(default=datetime.datetime(2015, 5, 16, 13, 51, 54, 33558), verbose_name='Дата події'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='event_type',
            field=models.CharField(default='Л', max_length=1, choices=[('Л', 'Літургія'), ('В', 'Весілля'), ('П', 'Похорон')], verbose_name='Тип події'),
            preserve_default=True,
        ),
    ]
