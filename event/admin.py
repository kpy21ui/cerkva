from django.contrib import admin
from event.models import Event


class EventAdmin(admin.ModelAdmin):
    list_display = ["__str__", "event_date", "event_time", "event_description"]
    ordering = ["event_date", "event_time"]

admin.site.register(Event, EventAdmin)