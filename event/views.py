from collections import OrderedDict
from django.db.models import Q
from django.views.generic import ListView
from .models import Event


class EventView(ListView):
    context_object_name = 'events'
    model = Event
    template_name = 'event.html'

    def get_queryset(self):
        qs = None
        try:
            event_t = self.kwargs['event_t']
        except KeyError:
            event_t = 'liturgy'

        if event_t == 'liturgy':
            criterion1 = Q(event_type="Л")
            # criterion2 = Q(event_type="Р")
            # criterion3 = Q(event_type="Н")
            qs = Event.objects.filter(criterion1).order_by('event_date')
        elif event_t == 'wedding':
            qs = Event.objects.filter(event_type='В').order_by('event_date')
        elif event_t == 'funeral':
            qs = Event.objects.filter(event_type='П').order_by('event_date')

        evn = OrderedDict()
        for ev in qs:
            if ev.event_date not in evn.keys():
                evn[ev.event_date] = [(ev.event_description, ev.event_name, ev.event_time)]
            else:
                evn[ev.event_date].append((ev.event_description, ev.event_name, ev.event_time))
        qs = evn
        return qs
