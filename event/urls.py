from django.conf.urls import url
from .views import EventView


urlpatterns = [

    url(r'(?P<event_t>\w+)/$', EventView.as_view(), name="event"),

]
