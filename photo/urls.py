from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^(?P<pk>\d+)/$', views.PhotoAlbumDetailView.as_view(), name="album"),
    url(r'^$', views.PhotoAlbumsView.as_view(), name='albums'),
]
