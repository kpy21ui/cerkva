from django.contrib import admin
from photo.models import Album, Tag, Image


class AlbumshipInline(admin.StackedInline):
    exclude = ["albums"]
    model = Image.albums.through
    extra = 5


class AlbumAdmin(admin.ModelAdmin):
    fields = ('title', 'created', )
    list_display = ["title", "created"]
    search_fields = ["title"]
    exclude = ["albums"]
    inlines = [AlbumshipInline]


class ImageAdmin(admin.ModelAdmin):
    # search_fields = ["title"]
    list_display = ["__str__", "title", "size", "tags_", "albums_", "thumbnail", "created"]
    list_filter = ["tags", "albums"]
    exclude = ["thumbnail2", "width", "height"]


class TagAdmin(admin.ModelAdmin):
    list_display = ["tag"]


admin.site.register(Album, AlbumAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Tag, TagAdmin)
