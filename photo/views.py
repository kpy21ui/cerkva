from django.views.generic import ListView, DetailView
from cerkva.settings import MEDIA_URL
from .models import Album


class PhotoAlbumsView(ListView):
    context_object_name = 'albums'
    model = Album
    template_name = 'albums.html'
    paginate_by = 12
    ordering = ['-created', ]

    def get_context_data(self, **kwargs):
        context = super(PhotoAlbumsView, self).get_context_data(**kwargs)
        context['media_url'] = MEDIA_URL

        for album in context.get('albums'):
            album.images = album.image_set.all()[:1]

        return context


class PhotoAlbumDetailView(DetailView):
    context_object_name = 'album'
    model = Album
    template_name = 'album.html'

    def get_context_data(self, **kwargs):
        context = super(PhotoAlbumDetailView, self).get_context_data(**kwargs)
        context['media_url'] = MEDIA_URL
        context['images'] = context.get('album').image_set.all()
        return context
