# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('photo', '0005_album_created'),
    ]

    operations = [
        migrations.AlterField(
            model_name='album',
            name='created',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2016, 5, 5, 7, 13, 24, 916401, tzinfo=utc)),
        ),
    ]
