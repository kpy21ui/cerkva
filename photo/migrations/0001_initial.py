# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('title', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name_plural': 'Альбоми',
                'verbose_name': 'Альбом',
                'db_table': 'album',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('title', models.CharField(blank=True, max_length=200, null=True)),
                ('image', models.FileField(upload_to='images/')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('width', models.ImageField(blank=True, upload_to='', null=True)),
                ('height', models.ImageField(blank=True, upload_to='', null=True)),
                ('albums', models.ManyToManyField(blank=True, to='photo.Album')),
            ],
            options={
                'verbose_name_plural': 'Фотографії',
                'verbose_name': 'Фотографія',
                'db_table': 'image',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('tag', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='image',
            name='tags',
            field=models.ManyToManyField(blank=True, to='photo.Tag'),
            preserve_default=True,
        ),
    ]
