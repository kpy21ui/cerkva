# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photo', '0003_auto_20150315_1631'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='image',
            field=models.FileField(blank=True, null=True, upload_to='images/photos/'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='image',
            name='thumbnail2',
            field=models.ImageField(blank=True, null=True, upload_to='images/photos/'),
            preserve_default=True,
        ),
    ]
