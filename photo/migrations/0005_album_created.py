# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photo', '0004_auto_20150315_1803'),
    ]

    operations = [
        migrations.AddField(
            model_name='album',
            name='created',
            field=models.DateTimeField(default='2016-05-04 12:00:00', auto_now_add=True),
            preserve_default=False,
        ),
    ]
