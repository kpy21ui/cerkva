from django.db import models
import os
from PIL import Image as PImage
from django.template.defaultfilters import join

from cerkva.settings import MEDIA_ROOT
from tempfile import NamedTemporaryFile
from django.core.files import File
from django.utils import timezone


class Album(models.Model):
    title = models.CharField(max_length=200)
    created = models.DateTimeField(default=timezone.now, blank=True)

    def images(self):
        lst = [x.image.name for x in self.image_set.all()]
        lst = ["<a href='/media/%s'>%s</a>" % (x, x.split('/')[-1]) for x in lst]
        return join(lst, ', ')
    images.allow_tags = True

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'album'
        verbose_name_plural = "Альбоми"
        verbose_name = "Альбом"


class Tag(models.Model):
    tag = models.CharField(max_length=50)

    def __str__(self):
        return self.tag


class Image(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True)
    image = models.FileField(upload_to="images/photos/", blank=True, null=True)
    thumbnail2 = models.ImageField(upload_to="images/photos/", blank=True, null=True)
    tags = models.ManyToManyField(Tag, blank=True)
    albums = models.ManyToManyField(Album, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)

    def save(self, *args, **kwargs):
        """seve image demensions"""
        try:
            old_name = Image.objects.get(id=self.id).image.name
            os.remove(MEDIA_ROOT+"/"+old_name)
        except Exception:
            pass
        super(Image, self).save(*args, **kwargs)
        im = PImage.open(os.path.join(MEDIA_ROOT, self.image.name))
        im.thumbnail((1200, 1200), PImage.ANTIALIAS)
        self.width, self.height = im.size
        im.save(MEDIA_ROOT+"/"+self.image.name, "JPEG")

        # large thumbnail
        fn, ext = os.path.splitext(self.image.name)
        im.thumbnail((260, 260), PImage.ANTIALIAS)
        thumb_fn = fn + "-thumb2" + ext
        tf2 = NamedTemporaryFile()
        im.save(tf2.name, "JPEG")
        self.thumbnail2.save(thumb_fn, File(open(tf2.name, 'rb')), save=False)
        tf2.close()

        ## small thumbnail
        #im.thumbnail((40, 40), PImage.ANTIALIAS)
        #thumb_fn = fn + "-thumb" + ext
        #tf = NamedTemporaryFile()
        ##tf_name = tf.name.encode('utf-8')
        #im.save(tf.name, "JPEG")
        #self.thumbnail.save(thumb_fn, File(open(tf.name, 'rb')), save=False)
        #tf.close()

        super(Image, self).save(*args, **kwargs)

    def delete(self, save=True):
        f_path = os.path.join(MEDIA_ROOT, self.image.name)
        if os.path.exists(self.f_path):
            os.remove(self.f_path)
        super(Image, self).delete(save)

    def size(self):
        """Image size"""
        return "%s x %s" % (self.width, self.height)

    def __str__(self):
        return self.image.name

    def tags_(self):
        lst = [x[1] for x in self.tags.values_list()]
        return ', '.join(str(e) for e in lst)

    def albums_(self):
        lst = [x[1] for x in self.albums.values_list()]
        return ', '.join(str(e) for e in lst)

    def thumbnail(self):
        return """<a href="/media/%s"><img border="0" alt="" src="/media/%s" height="40" /></a>""" % ((self.image.name, self.image.name))
    thumbnail.allow_tags = True

    class Meta():
        db_table = 'image'
        verbose_name_plural = "Фотографії"
        verbose_name = "Фотографія"
