# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0003_auto_20150116_0916'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prior',
            name='prior_image',
            field=models.ImageField(upload_to='images/about/'),
            preserve_default=True,
        ),
    ]
