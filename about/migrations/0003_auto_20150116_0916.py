# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0002_prior_prior_tag'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='prior',
            table='prior',
        ),
    ]
