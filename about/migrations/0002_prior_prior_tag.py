# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='prior',
            name='prior_tag',
            field=models.TextField(verbose_name='Посилання', blank=True, null=True),
            preserve_default=True,
        ),
    ]
