# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Prior',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('prior_position', models.CharField(verbose_name='Посада', max_length=50)),
                ('prior_short_discription', models.TextField(verbose_name='Короткий опис')),
                ('prior_biography', models.TextField(verbose_name='Біографія')),
                ('prior_image', models.ImageField(upload_to='images/')),
            ],
            options={
                'verbose_name': 'Настоятель',
                'verbose_name_plural': 'Настоятелі',
            },
            bases=(models.Model,),
        ),
    ]
