from django.views.generic import ListView, DetailView
from cerkva.settings import MEDIA_URL
from about.models import Prior


class PriorsView(ListView):
    context_object_name = 'priors'
    model = Prior
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        context = super(PriorsView, self).get_context_data(**kwargs)
        context['media_url'] = MEDIA_URL
        return context


class PriorDetailView(DetailView):
    context_object_name = 'prior'
    model = Prior
    template_name = 'prior.html'

    def get_context_data(self, **kwargs):
        context = super(PriorDetailView, self).get_context_data(**kwargs)
        context['media_url'] = MEDIA_URL
        return context
