from django.conf.urls import url
from .views import PriorsView, PriorDetailView


urlpatterns = [
    url(r'^(?P<pk>\d+)/$', PriorDetailView.as_view(), name="prior"),
    url(r'^$', PriorsView.as_view(), name="priors"),
]
