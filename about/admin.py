from django.contrib import admin
from about.models import Prior


class AdminPrior(admin.ModelAdmin):
    list_display = ["prior_position", "prior_image"]

admin.site.register(Prior, AdminPrior)