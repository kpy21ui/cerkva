from django.db import models
from cerkva.settings import MEDIA_URL
from tinymce.models import HTMLField


class Prior(models.Model):
    prior_position = models.CharField(max_length=50, verbose_name="Посада")
    prior_short_discription = models.TextField(verbose_name="Короткий опис")
    prior_biography = HTMLField(verbose_name="Біографія")
    prior_image = models.ImageField(upload_to="images/about/")
    prior_tag = models.TextField(blank=True, null=True, verbose_name="Посилання")

    def __str__(self):
        return self.prior_position

    class Meta():
        db_table = "prior"
        verbose_name_plural = "Настоятелі"
        verbose_name = "Настоятель"